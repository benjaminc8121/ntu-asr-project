#!/bin/bash
set -u

NAMESPACE=kaldi-test
KUBE_NAME=kaldi-feature-test
PUBLIC_IP=52.187.104.49

NGINX_CONTROLLER=nginx-sticky-ingress-nginx-controller
GRAFANA=grafana
MASTER_CONTROLLER=$KUBE_NAME-master

# Patch services to expose to the public IP address
kubectl patch svc $NGINX_CONTROLLER  -n $NAMESPACE -p '{"spec": {"type": "LoadBalancer", "externalIPs":["'$PUBLIC_IP'"]}}'
kubectl patch svc $GRAFANA  -n $NAMESPACE -p '{"spec": {"type": "LoadBalancer", "externalIPs":["'$PUBLIC_IP'"]}}'
kubectl patch svc $MASTER_CONTROLLER  -n $NAMESPACE -p '{"spec": {"type": "LoadBalancer", "externalIPs":["'$PUBLIC_IP'"]}}'

kubectl create -f public-grafana-np.yml
kubectl create -f public-master-node.yml
kubectl create -f public-nginx.yml

echo "Done"
# 
# Now you can access the service via $PUBLIC_IP:$PUBLIC_PORT.
# To get your service external ports, you can use the command: 
# kubectl describe service <grafana> | grep -i nodeport
# You also need to open the firewall to enable external ports.
# 


