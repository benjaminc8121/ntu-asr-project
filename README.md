# Set up ASR application on AZURE
1. Open terminal
2. Go to Project Directory
3. `cd Terraform_azure`
## Create access token for Gitlab terraform backend
4. Log into Gitlab
5. Click the Drop down on the top right hand corner
6. Go to **Preferences > Access Tokens**
7. Create a token named Terraform with api access
8. Save the **Terraform Token**


## Set up Azure Infrastructure using Terraform
13. Run `az login` so Terraform is authenticated with Azure
14. Run the following in **Terraform_azure** directory: <br/>
```
terraform init --reconfigure\
    -backend-config="address=https://gitlab.com/api/v4/projects/<Project_ID>/terraform/state/production_azure" \
    -backend-config="username=<Gitlab Username>" \
    -backend-config="password=<Terraform Access Token>"
terraform validate
terraform plan
terraform apply -auto-approve
```
15. Wait while Terraform configures your infrastructure
 
## Deploy ASR application on Azure
16. Run the following to set up Environment: <br />
```
export GITLAB_USERNAME=<GITLAB_USERNAME>
export GITLAB_PASSWORD=<PASSWORD>
export GITLAB_EMAIL=<GITLAB_EMAIL>
export KUBE_NAME=sgdecoding-online-scaled
export NAMESPACE=ntuasr-production-azure
export RESOURCE_GROUP=ntu-online-scaled
export STORAGE_ACCOUNT_NAME=ntuscaledstorage3
export MODEL_SHARE=online-models
export MODELS_FILESHARE_SECRET="models-files-secret"
export STORAGE_KEY=$(az storage account keys list \
  --resource-group $RESOURCE_GROUP\
  --account-name $STORAGE_ACCOUNT_NAME \
  --query "[0].value" -o tsv)
echo Storage account name: $STORAGE_ACCOUNT_NAME
echo Storage account key: $STORAGE_KEY<br>
```
_Note: We are using Gitlab container Registry to store our container image_

17. Download all the files this link and save it in **models/** directory: <br>
 https://www.dropbox.com/sh/fnfknblof219ngl/AAAHOPxQJ2FOK6Av1XQSj--Qa?dl=0 <br><br>
 _Note: Ensure the the directory structure is the same_
 
18. Upload the models onto Azure file store using the following command: <br>
```
NUM_MODELS=$(find ./models/ -maxdepth 1 -type d | wc -l)
if [ $NUM_MODELS -gt 1 ]; then
    echo "Uploading models to storage..."
    # az storage blob upload-batch -d $AZURE_CONTAINER_NAME --account-key $STORAGE_KEY --account-name $STORAGE_ACCOUNT_NAME -s models/
    az storage file upload-batch -d $MODEL_SHARE --account-key $STORAGE_KEY --account-name $STORAGE_ACCOUNT_NAME -s models/
else
    printf "\n"
    printf "##########################################################################\n"
    echo "Please put at least one model in the ./models directory before continuing"
    printf "##########################################################################\n"
    exit 1
fi
echo "$((NUM_MODELS - 1)) models uploaded to Azure File Share storage | Azure Files: $MODEL_SHARE"
```

19. Connect to the Kubernetes cluster by running: <br>
`az aks get-credentials --resource-group $RESOURCE_GROUP --name asr-production --overwrite-existing`
20. Set up namespace for our application and go to that namespace: <br>
```
kubectl create namespace $NAMESPACE
kubectl config set-context --current --namespace $NAMESPACE
```
21. Create Fileshare secret for cluster:
```
kubectl create secret generic $MODELS_FILESHARE_SECRET \
    --from-literal=azurestorageaccountname=$STORAGE_ACCOUNT_NAME \
    --from-literal=azurestorageaccountkey=$STORAGE_KEY
```
22. Apply Kubernetes secrets:
```
kubectl apply -f azure_deployment_helm/secret/run_kubernetes_secret.yaml
```
23. Apply persistant volumes configurations:
```
kubectl apply -f azure_pv/
```
24. Create Container Registry Credentials using Kubernetes secrets:
```
kubectl create secret docker-registry regcred 
--docker-server=registry.gitlab.com 
--docker-username=$GITLAB_USERNAME 
--docker-password=$GITLAB_PASSWORD 
--docker-email=$GITLAB_EMAIL
```
25. Deploy application using Helm:
```
helm install $KUBE_NAME azure_deployment_helm/helm/sgdecoding-online-scaled/
```
26. Monitor Master and worker pods using:
```
kubectl get pods -w
```
27. Once the pods are running, test the application using:
```
export MASTER_SERVICE="$KUBE_NAME-master"
export MASTER_SERVICE_IP=$(kubectl get svc $MASTER_SERVICE \
    --output jsonpath='{.status.loadBalancer.ingress[0].ip}')
python3 client/client_3_ssl.py -u ws://$MASTER_SERVICE_IP/client/ws/speech -r 32000 -t abc --model="SingaporeCS_0519NNET3" client/audio/episode-1-introduction-and-origins.wav
```

# Set up ASR application on Google Cloud

## Set up Google Infrastructure using Terraform
1. Install Gcloud CLI
2. Run `gcloud auth application-default login` so Terraform is authenticated with Google Cloud
3. Go to Google Cloud console: https://console.cloud.google.com/
4. Create a project and copy the project ID
5. Set project to the project created
```
export PROJECT_ID=project-name-xxxxxx
gcloud config set project $PROJECT_ID
```
5. Enable APIs
```
gcloud services enable container.googleapis.com
gcloud services enable file.googleapis.com
```
6. Go to **Terraform_google/providers.tf** 
7. Fill up/replace the following:<br />
```
terraform { 
    backend "azurerm" { 
        resource_group_name = "terraform-group 
        storage_account_name = "terraform-storage 
        container_name = "tfstate 
        key = "prod.google.tfstate 
        sas_token = "sp=racwdl&st=2021-08-29T07:35:55Z&se=2021-12-31T15:35:55Z&sv=2020-08-04&sr=c&sig=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 
    }
}
```
_Note: We are same backend storage as Azure, just the key is different_

8. Run the following in **Terraform_google** directory: <br/>
```
terraform init --reconfigure\
    -backend-config="address=https://gitlab.com/api/v4/projects/<Project_ID>/terraform/state/production_azure" \
    -backend-config="username=<Gitlab Username>" \
    -backend-config="password=<Terraform Access Token>"
terraform validate
terraform plan
terraform apply -auto-approve
```
9. Wait while Terraform configures your infrastructure

## Deploy ASR application on Google Cloud

1. Upload model to Google File Store
 - Go to Gcloud console **Compute Engine > VM instances** and click one of the VMs
 - Get the code to ssh to the VM
 - ssh to the VM using a new terminal window
 - Run the following to mount the filestore onto the VM
``` 
 mkdir mnt 
 sudo mount <filstore ip>:/<filestore path> <mount directory>
 sudo chmod go+rw mnt
 pwd
```
 - Keep the output from the pwd command
 - Upload the models by running the following in our project root directory:
 ```
gcloud compute scp models/SingaporeCS_0519NNET3 <VM_ID>:<output_from_pwd> --project=<PROJECT_ID> --zone=asia-southeast1-a --recurse
gcloud compute scp models/SingaporeMandarin_0519NNET3 <VM_ID>:<output_from_pwd> --project=<PROJECT_ID> --zone=asia-southeast1-a --recurse
 ```
_Note: VM_ID looks something like this gke-gke-ntu-asr-clus-ntu-asr-node-poo-5a093a1f-fcd2_

2. Connect to Kubernetes Cluster on Google Cloud 
```
gcloud container clusters get-credentials gke-ntu-asr-cluster --zone asia-southeast1-a --project $PROJECT_ID
```
3. Run the following to set up Environment: <br />
```
export GITLAB_USERNAME=<GITLAB_USERNAME>
export GITLAB_PASSWORD=<PASSWORD>
export GITLAB_EMAIL=<GITLAB_EMAIL>
export KUBE_NAME=sgdecoding-online-scaled
export NAMESPACE=ntuasr-production-google
```
_Note: We are using Gitlab container Registry to store our container image_

4. Set up namespace for our application and go to that namespace: <br>
```
kubectl create namespace $NAMESPACE
kubectl config set-context --current --namespace $NAMESPACE
```
5. Apply Kubernetes secrets:
```
kubectl apply -f google_deployment_helm/secret/run_kubernetes_secret.yaml
```
6. Apply persistant volumes configurations:
```
kubectl apply -f google_pv/
```
7. Create Container Registry Credentials using Kubernetes secrets:
```
kubectl create secret docker-registry regcred 
--docker-server=registry.gitlab.com 
--docker-username=$GITLAB_USERNAME 
--docker-password=$GITLAB_PASSWORD 
--docker-email=$GITLAB_EMAIL
```
8. Deploy application using Helm:
```
helm install $KUBE_NAME google_deployment_helm/helm/sgdecoding-online-scaled/
```
9. Monitor Master and worker pods using:
```
kubectl get pods -w
```
10. Once the pods are running, test the application using:
```
export MASTER_SERVICE="$KUBE_NAME-master"
export MASTER_SERVICE_IP=$(kubectl get svc $MASTER_SERVICE \
    --output jsonpath='{.status.loadBalancer.ingress[0].ip}')
python3 client/client_3_ssl.py -u ws://$MASTER_SERVICE_IP/client/ws/speech -r 32000 -t abc --model="SingaporeCS_0519NNET3" client/audio/episode-1-introduction-and-origins.wav
```

# Set up ASR application on AWS
## Set up AWS Infrastructure using Terraform
1. Install AWS CLI and EKSCTL
2. Run `aws configure` to login to AWS
3. Fill in Credentials
4. Go to **Terraform_aws/vpc.tf** 
5. Fill up/replace the following:<br />
```
terraform { 
    backend "azurerm" { 
        resource_group_name = "terraform-group 
        storage_account_name = "terraform-storage 
        container_name = "tfstate 
        key = "prod.aws.tfstate 
        sas_token = "sp=racwdl&st=2021-08-29T07:35:55Z&se=2021-12-31T15:35:55Z&sv=2020-08-04&sr=c&sig=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX 
    }
}
```
_Note: We are same backend storage as Azure, just the key is different_

6. Run the following in **Terraform_google** directory: <br/>
```
terraform init --reconfigure\
    -backend-config="address=https://gitlab.com/api/v4/projects/<Project_ID>/terraform/state/production_azure" \
    -backend-config="username=<Gitlab Username>" \
    -backend-config="password=<Terraform Access Token>"
terraform validate
terraform plan
terraform apply -auto-approve
```
7. Wait while Terraform configures your infrastructure

## Deploy ASR application on AWS
1. Upload models to EFS
 - Create a Key pair on AWS console
 - Create a new public subnet within the same VPC as the EFS
 - Launch an EC2 instance (micro will do) within the subnet we created
 - Mount the file system onto the EC2 instance
 - Include the Key pair we created in the EC2 instance
 - Run `scp -i "<Key_pair.pem>" -r models/ ec2-user@ec2-13-212-123-143.ap-southeast-1.compute.amazonaws.com:/mnt/efs/fs1` in project root directory
 - Wait for models to be uploaded
2. Run the following to set up Environment: <br />
```
export GITLAB_USERNAME=<GITLAB_USERNAME>
export GITLAB_PASSWORD=<PASSWORD>
export GITLAB_EMAIL=<GITLAB_EMAIL>
export KUBE_NAME=sgdecoding-online-scaled
export NAMESPACE=ntuasr-production-aws
export CLUSTERNAME=asr_cluster
```
_Note: We are using Gitlab container Registry to store our container image_

3. Create an IAM OIDC identity provider for your cluster: <br />
`eksctl utils associate-iam-oidc-provider --cluster $CLUSTERNAME --approve`
4. Connect to K8 Cluster: <br />
`aws eks --region ap-southeast-1 update-kubeconfig --name $CLUSTERNAME`
5. Create and set namespace <br />
```
kubectl create namespace $NAMESPACE
kubectl config set-context --current --namespace $NAMESPACE
```
6. deploy the EFS driver <br />
```
helm repo add aws-efs-csi-driver https://kubernetes-sigs.github.io/aws-efs-csi-driver/
helm repo update
helm upgrade -i aws-efs-csi-driver aws-efs-csi-driver/aws-efs-csi-driver \
  --namespace kube-system \
  --set image.repository=602401143452.dkr.ecr.ap-southeast-1.amazonaws.com/eks/aws-efs-csi-driver \
  --set controller.serviceAccount.create=false \
  --set controller.serviceAccount.name=efs-csi-controller-sa
```
7. Apply Kubernetes secrets:
```
kubectl apply -f aws_deployment_helm/secret/run_kubernetes_secret.yaml
```
8. Apply persistant volumes configurations:
```
kubectl apply -f aws_pv/
```
9. Create Container Registry Credentials using Kubernetes secrets:
```
kubectl create secret docker-registry regcred 
--docker-server=registry.gitlab.com 
--docker-username=$GITLAB_USERNAME 
--docker-password=$GITLAB_PASSWORD 
--docker-email=$GITLAB_EMAIL
```
10. Deploy application using Helm:
```
helm install $KUBE_NAME aws_deployment_helm/helm/sgdecoding-online-scaled/
```
11. Monitor Master and worker pods using:
```
kubectl get pods -w
```
12. Once the pods are running, test the application using:
```
export MASTER_SERVICE="$KUBE_NAME-master"
export MASTER_SERVICE_IP=$(kubectl get svc $MASTER_SERVICE \
    --output jsonpath='{.status.loadBalancer.ingress[0].ip}')
python3 client/client_3_ssl.py -u ws://$MASTER_SERVICE_IP/client/ws/speech -r 32000 -t abc --model="SingaporeCS_0519NNET3" client/audio/episode-1-introduction-and-origins.wav
```

# Connect Kubernetes Clusters to Gitlab
At the time when I was working on the project, I used this: https://docs.gitlab.com/ee/user/project/clusters/add_existing_cluster.html

However it seems to be deprecated. So please explore this option: https://docs.gitlab.com/ee/user/clusters/agent/index.html

## Automate Azure Terraform Authentication
1. create service account For Terraform Authentication
```
az ad sp create-for-rbac --name TerraformServicePrincipal
```
_Output looks something like this_:
```
{
  "appId": "<appID>",
  "displayName": "TerraformServicePrincipal",
  "name": "http://TerraformServicePrincipal",
  "password": "<password>",
  "tenant": "<tenantID>"
}
```
2. Go to **Gitlab Project > Settings > CI/CD > Variables > Expand > Add Variable**
3. Create the following Credentials and set as **Protected**:

 - **Key** - ARM_CLIENT_ID, **Value** - appID (Get from output above)
 - **Key** - ARM_CLIENT_SECRET, **Value** - password (Get from output above)
 - **Key** - ARM_SUBSCRIPTION_ID, **Value** - subscriptio_ID (Get from Azure Console)
 - **Key** - ARM_TENANT_ID, **Value** - tenantID (Get from output above)
 4. After doing this, the CI/CD pipeline will be able to configure Azure using Terraform.

 ## Automate Google Cloud Terraform Authentication
1. Go to **IAM & Admin > ServiceAccounts > terraform-sa(created using terraform) > Keys** on Google Cloud Console
2. Create a new JSON type key
3. Save the Service account credentials file somewhere
4. Upload Credentials file as Secret file on Gitlab:
 - Go to **Gitlab Project > Settings > CI/CD > Variables > Expand > Add Variable**
 - **Type** set to **file**
 - Copy over the contents of the file to **Value**
 - **Key**: Google-Credentials
5. Create Terraform role by running the following in **Terraform_google** directory: <br />
```
 gcloud iam roles create Terraform_role \
  --file=Terraform_role.yaml \
  --project $PROJECT_ID
```
6. Bind Terraform role to Service account created earlier: <br />
```
gcloud projects add-iam-policy-binding ntu-asr-317615 \
    --member=serviceAccount:<Client_email> \
    --role=projects/<project ID>/roles/Terraform_role
```
_Note: Client_email is found in the Credentials file_

7. After doing this, the CI/CD pipeline will be able to configure Gcloud using Terraform.

## Automate AWS Terraform Authentication
1. Go to **Gitlab Project > Settings > CI/CD > Variables > Expand > Add Variable**
2. Create the following Credentials and set as **Protected**:
 - **Key** - AWS_ACCESS_KEY_ID, **Value** - XXXXXXXXEXAMPLE
 - **Key** - AWS_SECRET_ACCESS_KEY, **Value** - XXXXXX/XXXXXX/XXXXXEXAMPLEKEY
 3. After doing this, the CI/CD pipeline will be able to configure AWS using Terraform.

# CI/CD Pipeline configurations
- The Pipeline is built in the **.gitlab-ci.yaml** file in the root directory of this project.

# References

https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/

https://docs.gitlab.com/ee/user/project/clusters/add_existing_cluster.html

https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret

https://cloud.google.com/filestore/docs/mounting-fileshares?_ga=2.46441251.-588590073.1621505219&_gac=1.196300382.1621589414.CjwKCAjwtJ2FBhAuEiwAIKu19sY34QTBMjnuOPiA3WTrbGoMW2Y52rhApkQOk6eK7-LOfHczeQSaERoCy98QAvD_BwE#debianubuntu

https://cloud.google.com/filestore/docs/copying-data


https://aws.amazon.com/premiumsupport/knowledge-center/eks-worker-nodes-cluster/


https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstancesLinux.html

Setup EFS: https://docs.aws.amazon.com/eks/latest/userguide/efs-csi.html
